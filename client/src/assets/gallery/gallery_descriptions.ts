export interface GalleryItem {
  url: string;
  description: string;
}

export const gallery_images = [
  {
    name: "reno_1.jpg",
    url: "/src/assets/gallery/reno_1.jpg",
    description: "Description for Reno 1",
  },
  {
    name: "reno_2.jpg",
    url: "/src/assets/gallery/reno_2.jpg",
    description: "Description for Reno 2",
  },
  {
    name: "reno_3.jpg",
    url: "/src/assets/gallery/reno_3.jpg",
    description: "Description for Reno 3",
  },
  {
    name: "reno_4.jpg",
    url: "/src/assets/gallery/reno_4.jpg",
    description: "Description for Reno 4",
  },
  {
    name: "reno_5.jpg",
    url: "/src/assets/gallery/reno_5.jpg",
    description: "Description for Reno 5",
  },
  {
    name: "painting_1.jpg",
    url: "/src/assets/gallery/painting_1.jpg",
    description: "Description for Painting 1",
  },
];
