import navStyles from "../styles/Nav.module.css"
import { Link } from "react-router-dom";

interface NavProps {
  active: string;
}

const Nav = ({ active }: NavProps) => {
  return (
    <nav className={navStyles.navBar}>
      <ul>
        <li className={active == "services" ? `${navStyles.active}` : ''}><Link to="/services">SERVICES</Link></li>
        <li className={active == "contact" ? `${navStyles.active}` : ''} ><Link to="/contact">CONTACT</Link></li>
        <li className={active == "gallery" ? `${navStyles.active}` : ''} ><Link to="/gallery">GALLERY</Link></li>
      </ul>
    </nav>
  )
}

export default Nav
