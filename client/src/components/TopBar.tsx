import styles from "../styles/TopBar.module.css"
import { Link } from 'react-router-dom';
import { FaEnvelope } from "react-icons/fa";
import { FaPhone } from "react-icons/fa";

const TopBar = () => {
  return (
    <div className={styles.backgroundContainer}>
      <div className={styles.container}>
        <a href="mailto:charlie@sloanspaintnfix.ca" className={styles.icon}><FaEnvelope></FaEnvelope></a>
        <a href="tel:9059022975" className={styles.icon}><FaPhone></FaPhone></a>
        <Link className={styles.button} to='/contact'>Get Free Estimate</Link>
      </div>
    </div>
  )
}

export default TopBar
