import { Link } from "react-router-dom";
import styles from "../styles/App.module.css";
import headerStyles from "../styles/Header.module.css";
import buttonStyles from "../styles/Button.module.css";
import { motion } from "framer-motion";
import { FaChevronCircleDown } from "react-icons/fa";

interface Props {
  servicesRef: React.MutableRefObject<HTMLElement | null>
}

const Header = ({ servicesRef }: Props) => {

  const scrollTo = () => {
    if (servicesRef.current !== null) {
      servicesRef.current.scrollIntoView({ behavior: "smooth" })
    }
  }
  return (
    <div className={headerStyles.fullWidth}>
      <section className={`${styles.container} ${headerStyles.heroSection}`}>
        <h1 className={`${headerStyles.headingText}`}>
          Quality{" "}
          <em>
            <span className={headerStyles.rustAccent}>Painting</span>
          </em>{" "}
          and{" "}
          <em>
            <span className={headerStyles.rustAccent}>Renovations</span>
          </em>{" "}
          services in the Hamilton area
        </h1>
        <Link to={{ pathname: "/contact" }} state={{ scrollToForm: true }}>
          <motion.button
            className={buttonStyles.button}
            disabled={false}
            animate={{ scale: [1, 1.2, 1] }}
            transition={{
              duration: 0.2,
              repeat: Infinity,
              repeatDelay: 2,
            }}
          >
            Get Free Quote
          </motion.button>
        </Link>

        {/* THE LEARN MORE BUTTON DUDE*/}
        <div className={buttonStyles.learnMoreContainer}>
          <motion.p
            className={buttonStyles.noBorderButton}
          >
            Learn More
          </motion.p>
          <motion.div
            animate={{ y: [0, 5, 0] }}
            whileTap={{ scale: 0.8 }}
            whileHover={{ scale: 1.2 }}
            transition={{
              duration: 0.2,
              repeat: Infinity,
              repeatDelay: 2,
            }}
          >
            <FaChevronCircleDown
              onClick={scrollTo}
              className={buttonStyles.chevron}
            ></FaChevronCircleDown>
          </motion.div>
        </div>
      </section>
    </div>
  );
};

export default Header;
