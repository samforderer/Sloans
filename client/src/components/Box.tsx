import mainStyles from "../styles/App.module.css";

interface CardProps {
  children?: React.ReactNode;
  className?: React.CSSProperties;
}

const Services = ({ children, className }: CardProps) => {
  return (
    <section className={`${mainStyles.greyContainer}`} style={className}>
      {children}
    </section>
  );
};

export default Services;
