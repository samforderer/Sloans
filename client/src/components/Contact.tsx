import React from "react";
import { useLocation } from "react-router-dom";
import Card from "./Card";
import Form from "./Form";
import contactStyles from "../styles/Contact.module.css";
import typo from "../styles/Typography.module.css";
import servicesStyles from "../styles/Services.module.css";
import mainStyles from "../styles/App.module.css";
import { FaEnvelope, FaPhone } from "react-icons/fa";
import Map from "../components/Map";

const Contact = () => {
  const { state } = useLocation();
  const scrollToForm = state ? state.scrollToForm : false;
  console.log(scrollToForm);

  const quoteForm = React.useRef<HTMLDivElement>(null);

  const scroll = () => {
    if (scrollToForm && quoteForm.current && window.innerWidth < 600) {
      window.scrollTo({ top: quoteForm.current.offsetTop, behavior: "smooth" });
    }
  };

  React.useEffect(() => {
    scroll();
  }, []);

  return (
    <section className={`${contactStyles.container}`}>
      <div>
        <Card>
          <div className={contactStyles.contactCard}>
            <div className={contactStyles.avi}></div>
            <div className={contactStyles.contactCardType}>
              <h1 className={mainStyles.heading1}>Charlie Sloan</h1>
              <p>Owner/General Contractor</p>
            </div>
          </div>
          <p
            className={typo.bodyText}
            style={{ width: "80%", lineHeight: "2", marginTop: "50px" }}
          >
            Hey there! I'm Charlie, owner and contractor at Sloan's Paint 'n
            Fix. I'm looking forward to talking about the job you need done.
            Please feel free to call or email us using the button below, or fill
            out the quote form here to get your service started!
          </p>
        </Card>
        <Card>
          <div className={contactStyles.iconsContainer}>
            <div className={servicesStyles.iconBox}>
              <a href="tel:9059022975" className={contactStyles.link}>
                <FaPhone className={servicesStyles.icon}></FaPhone>
                <h2>Call</h2>
              </a>
            </div>
            <div className={servicesStyles.iconBox}>
              <a
                href="mailto:charlie@sloanspaintnfix.ca"
                className={contactStyles.link}
              >
                <FaEnvelope className={servicesStyles.icon}></FaEnvelope>
                <h2>Email</h2>
              </a>
            </div>
          </div>
        </Card>
        <Map></Map>
      </div>
      <div ref={quoteForm} id="quote">
        <Form />
      </div>
    </section>
  );
};

export default Contact;
