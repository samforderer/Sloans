import { useRef } from "react";
import { MapContainer, TileLayer, Circle } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import styles from "../styles/Map.module.css";
import base from "../styles/App.module.css";

const Map = () => {
  const mapRef = useRef(null);

  return (
    <>
      <h1 className={base.heading2}>Service Area</h1>
      <MapContainer
        className={styles.map}
        center={[43.2557, -79.871]}
        zoom={10}
        scrollWheelZoom={false}
        style={{
          width: "100%",
          height: "250px",
        }}
        ref={mapRef}
      >
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Circle
          center={[43.2557, -79.871]}
          pathOptions={{ color: "orange", fillColor: "orange" }}
          radius={12000}
        ></Circle>
      </MapContainer>
    </>
  );
};

export default Map;
