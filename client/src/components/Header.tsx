import headerStyles from "../styles/Header.module.css";

import logo from "../assets/logo200.png";

import { Link } from "react-router-dom";
import Nav from "./Nav";
import TopBar from "./TopBar";
import MobileNav from "./MobileNav";

interface HeaderProps {
  activePage: "services" | "contact" | "home" | "gallery";
}

const Header = ({ activePage }: HeaderProps) => {
  return (
    <>
      <TopBar></TopBar>
      <header className={headerStyles.header}>
        <Link to="/">
          <img src={logo} className={headerStyles.logo} />
        </Link>
        <Nav active={activePage}></Nav>
      </header>
      <MobileNav active={activePage}></MobileNav>
    </>
  );
};

export default Header;
