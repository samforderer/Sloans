import React from "react";
import base from "../../styles/App.module.css";
import services from "../../styles/Services.module.css";
import { FaHammer, FaPaintRoller } from "react-icons/fa";

interface Props {
  servicesRef: React.MutableRefObject<HTMLElement | null>;
}

const ServicesHome = ({ servicesRef }: Props) => {
  return (
    <section
      id="services"
      className={`${base.greyContainer} ${base.container}`}
      ref={servicesRef}
    >
      <h1 className={base.heading1}>Our Services</h1>
      <div className={services.servicesIconContainer}>
        <div className={services.iconBox}>
          <FaPaintRoller className={`${services.icon}`}></FaPaintRoller>
          <h2 className={services.heading}>Painting</h2>
        </div>
        <div className={services.iconBox}>
          <FaHammer className={`${services.icon}`}></FaHammer>
          <h2 className={services.heading}>Renovations</h2>
        </div>
      </div>
      <p className={services.servicesPara}>
        Our painting services offer a transformative touch to your spaces,
        whether commercial or residential, infusing them with vibrant colors and
        renewed vitality. Meanwhile, our renovation services breathe new life
        into your properties, enhancing functionality and aesthetics with expert
        craftsmanship and attention to detail. Trust us to revitalize your
        spaces and bring your vision to life with precision and professionalism.
      </p>
    </section>
  );
};

export default ServicesHome;
