import ServicesSection from "./ServicesSection";
import painting from "../../assets/painting1.webp";
import renos from "../../assets/reno1.webp";

const paintingText =
  "Refresh your spaces effortlessly with our professional painting services designed for both commercial and residential properties. Our skilled team at Sloan's Paint 'n Fix specializes in delivering top-notch results, whether it's enhancing your business's curb appeal or adding a touch of style to your home's interior.";
const renoText =
  "Revitalize your home or business with expert renovation services from Sloan's Remodels. Whether it's updating your living space for comfort or revitalizing your commercial property for functionality, our skilled team is dedicated to delivering exceptional results. ";

const Services = () => {
  return (
    <>
      <ServicesSection
        heading="Painting"
        body={paintingText}
        bgImage={painting}
        style={{ marginBottom: "50px" }}
        columns="1/4"
        buttonText="View Painting Gallery"
        buttonPosition="right"
        linkPath="/gallery"
        linkState="painting"
      ></ServicesSection>

      <ServicesSection
        heading="Renovations"
        body={renoText}
        bgImage={renos}
        columns="2/5"
        buttonText="View Renovations Gallery"
        buttonPosition="left"
        linkPath="/gallery"
        linkState="reno"
      ></ServicesSection>
    </>
  );
};

export default Services;
