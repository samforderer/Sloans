import React from "react";
import styles from "../../styles/Services.module.css";
import Box from "../Box";
import Button from "../Button";
import typo from "../../styles/Typography.module.css";
import { Link } from "react-router-dom";

interface ServicesSectionProps {
  bgImage: string;
  heading: string;
  body: string;
  style?: React.CSSProperties;
  columns?: string;
  buttonText: string;
  buttonPosition?: "left" | "right";
  linkPath: string;
  linkState: string;
}

const ServicesSection = ({
  heading,
  body,
  style,
  bgImage,
  columns,
  buttonText,
  buttonPosition,
  linkPath,
  linkState,
}: ServicesSectionProps) => {
  return (
    <>
      <section
        className={styles.container}
        style={{ backgroundImage: `url(${bgImage})`, ...style }}
      >
        <div className={styles.grid}>
          <Box
            className={{
              textAlign: "left",
              display: "flex",
              flexDirection: "column",
              boxSizing: "border-box",
              padding: "70px",
              margin: "0",
              gridColumn: columns,
            }}
          >
            <h1 className={typo.heading1Futura}>{heading}</h1>
            <p className={typo.bodyText}>{body}</p>
            <Link to={linkPath} state={{ initialFilter: linkState }}>
              <Button
                text={buttonText}
                disabled={false}
                position={buttonPosition}
              ></Button>
            </Link>
          </Box>
        </div>
      </section>
    </>
  );
};

export default ServicesSection;
