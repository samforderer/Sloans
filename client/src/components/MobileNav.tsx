import { Link } from "react-router-dom";
import { FaHome, FaImage, FaPhone, FaWrench } from "react-icons/fa";
import styles from "../styles/MobileNav.module.css";

interface MobileNavProps {
  active: "services" | "contact" | "home" | "gallery";
}

const handleScroll = () => {
  window.scroll(0, 0);
};

const MobileNav = ({ active }: MobileNavProps) => {
  return (
    <nav className={styles.nav}>
      <ul className={styles.list}>
        <li>
          <Link
            to="/"
            className={active == "home" ? `${styles.active}` : ""}
            onClick={handleScroll}
          >
            <FaHome />
            <span className={styles.listText}>Home</span>
          </Link>
        </li>
        <li>
          <Link
            to="/contact"
            className={active == "contact" ? `${styles.active}` : ""}
            onClick={handleScroll}
          >
            <FaPhone />
            <span className={styles.listText}>Contact</span>
          </Link>
        </li>
        <li>
          <Link
            to="/services"
            className={active == "services" ? `${styles.active}` : ""}
            onClick={handleScroll}
          >
            <FaWrench></FaWrench>
            <span className={styles.listText}>Services</span>
          </Link>
        </li>
        <li>
          <Link
            to="/gallery"
            className={active == "gallery" ? `${styles.active}` : ""}
            onClick={handleScroll}
          >
            <FaImage></FaImage>
            <span className={styles.listText}>Gallery</span>
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default MobileNav;
