import styles from "../../styles/Gallery.module.css";
import { FaArrowLeft } from "react-icons/fa";
import { GalleryItem } from "../../lib/types";
import { motion } from "framer-motion";

interface Props {
  setFullscreen: (fullscreen: boolean) => void;
  selectedImage: GalleryItem[];
}

const GalleryImageLarge = ({ setFullscreen, selectedImage }: Props) => {
  const description = selectedImage[0]?.description ?? "loading";
  const title = selectedImage[0]?.title ?? "loading";

  return (
    <>
      <div className={styles.imageNav}>
        <h1 className={styles.imageHeading}>{title}</h1>
        <motion.button
          whileTap={{ scale: 0.8 }}
          className={styles.backButton}
          onClick={() => setFullscreen(false)}
        >
          <FaArrowLeft className={styles.backIconButton}></FaArrowLeft>
          <span className={styles.buttonText}>BACK</span>
        </motion.button>
      </div>
      {selectedImage?.map((item) => (
        <img
          key={item.url}
          className={styles.fullscreenImage}
          src={item?.url}
        ></img>
      ))}
      <h1>Job Description</h1>
      <p style={{ fontSize: "1.3em" }}>{description}</p>
    </>
  );
};

export default GalleryImageLarge;
