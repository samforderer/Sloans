import Box from "../Box.tsx";
import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import { GalleryItem } from "../../lib/types";
import GalleryGrid from "./GalleryGrid.tsx";
import GalleryImageLarge from "./GalleryImageLarge.tsx";

interface Gallery {
  gallery: GalleryItem[];
}
console.log("hello world");
console.log(import.meta.env.VITE_API_ENDPOINT);
const API_ENDPOINT = `${import.meta.env.VITE_API_ENDPOINT}/services/gallery`;

const Gallery = () => {
  const { state } = useLocation();
  const initialFilter = state ? [state.initialFilter] : [];
  const [fullscreen, setFullscreen] = useState(false);
  const [selectedImage, setSelectedImage] = useState<GalleryItem[]>([]);
  const [gallery, setGallery] = useState<GalleryItem[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    setLoading(true);
    const fetchData = async (): Promise<void> => {
      const response = await fetch(API_ENDPOINT);
      const images: Gallery = await (response.json() as Promise<Gallery>);
      setGallery(images.gallery);
    };

    try {
      fetchData();
      setLoading(false);
    } catch (error) {
      throw new Error("Could not get data");
    }
  }, []);

  return (
    <>
      <Box className={{ padding: "2%" }}>
        {fullscreen ? (
          <GalleryImageLarge
            setFullscreen={setFullscreen}
            selectedImage={selectedImage}
          ></GalleryImageLarge>
        ) : (
          <>
            <h1 style={{ fontSize: "3em", margin: "0" }}>Gallery</h1>
            <GalleryGrid
              images={gallery}
              setSelectedImage={setSelectedImage}
              setFullscreen={setFullscreen}
              loading={loading}
              initialFilters={initialFilter}
            ></GalleryGrid>
          </>
        )}
      </Box>
    </>
  );
};

export default Gallery;
