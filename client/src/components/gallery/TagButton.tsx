import styles from "../../styles/Gallery.module.css";
import { motion } from "framer-motion";

interface Props {
  children: any;
  active: boolean;
  handleClick: () => void;
}

const TagButton = ({ children, handleClick, active }: Props) => {
  return (
    <motion.button
      className={
        active
          ? `${styles.activeTagButton} ${styles.tagButton}`
          : styles.tagButton
      }
      type="button"
      onClick={handleClick}
      whileTap={{
        scale: 0.8,
      }}
    >
      {children}
    </motion.button>
  );
};

export default TagButton;
