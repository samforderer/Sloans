import { useRef } from "react";
import { motion } from "framer-motion";
import styles from "../../styles/Gallery.module.css";
import { GalleryItem } from "../../lib/types";

interface Props {
  item: GalleryItem;
  setSelectedImage: () => void;
  setFullScreen: () => void;
  domain: string;
}

const GalleryThumbnail = ({
  item,
  setFullScreen,
  setSelectedImage,
  domain,
}: Props) => {
  // imageRefs
  const imageRef = useRef<HTMLImageElement>(null);
  const containerRef = useRef<HTMLDivElement>(null);

  return (
    <div
      className={styles.gridImage}
      key={item?.id}
      style={{
        position: "relative",
      }}
      onClick={() => {
        setSelectedImage();
        setFullScreen();
      }}
      ref={containerRef}
    >
      <span className={styles.textImageOverlay}>{item?.title}</span>

      <motion.img
        className={styles.thumbnailImg}
        initial={{
          opacity: 0,
          x: -100,
        }}
        animate={{
          opacity: 1,
          x: 0,
        }}
        whileTap={{
          scale: 0.95,
        }}
        whileHover={{
          scale: 1.05,
        }}
        src={`${domain}/${item.url}`}
        alt={item.url}
        ref={imageRef}
        onLoad={() => {
          if (imageRef.current && containerRef.current) {
            const initialHeight = imageRef.current.clientHeight;
            const initialWidth = imageRef.current.clientWidth;
            if (initialHeight > initialWidth) {
              imageRef.current.style.height = "400px";
              containerRef.current.style.gridRow = "span 2";
            } else {
              containerRef.current.style.gridRow = "span 1";
              imageRef.current.style.height = "200px";
            }
          }
        }}
      />
    </div>
  );
};

export default GalleryThumbnail;
