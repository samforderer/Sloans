import React from "react";
import styles from "../../styles/Gallery.module.css";
import { GalleryItem } from "../../lib/types";
import SkeletonImg from "../SkeletonImg";
import GalleryThumbnail from "./GalleryThumbnail";
import TagButton from "./TagButton";

interface Props {
  images: GalleryItem[];
  setSelectedImage: React.Dispatch<React.SetStateAction<GalleryItem[]>>;
  setFullscreen: (value: boolean) => void;
  loading: boolean;
  initialFilters: string[];
}

const API_ENDPOINT = import.meta.env.VITE_API_ENDPOINT;

function GalleryGrid({
  images,
  setSelectedImage,
  setFullscreen,
  loading,
  initialFilters,
}: Props) {

  const [filters, setFilters] = React.useState<string[]>(initialFilters);
  const [filteredImages, setFilteredImages] = React.useState<GalleryItem[]>([]);
  const [currentPage, setCurrentPage] = React.useState<number>(0);

  React.useEffect(() => {
    setFilteredImages(
      images.filter((item) => {
        return (
          item.url.split("_")[2][0] === "1" && // filter out only images that are the first of the project
          (filters.length === 0 ||
            filters.includes(item.url.split("/")[1].split("_")[0]))
        );
      })
    );
  }, [images, filters]);

  const handleTagToggle = (filter: string) => {
    if (!filters.includes(filter)) {
      setFilters([...filters, filter]);
    } else {
      setFilters(filters.filter((item) => item !== filter));
    }
    setCurrentPage(0);
  };

  if (loading) {
    return (
      <>
        <div>
          <SkeletonImg></SkeletonImg>
        </div>
        <div>
          <SkeletonImg></SkeletonImg>
        </div>
      </>
    );
  }

  return (
    <>
      <div className={styles.galleryGrid}>
        <div className={styles.grid}>
          <div className={styles.tagBox}>
            <h2>Filters</h2>
            <TagButton
              active={filters.includes("painting")}
              handleClick={() => {
                handleTagToggle("painting");
              }}
            >
              #painting
            </TagButton>
            <TagButton
              active={filters.includes("reno")}
              handleClick={() => {
                handleTagToggle("reno");
                console.log(filters);
              }}
            >
              #renos
            </TagButton>
          </div>
          <div className={styles.galleryColumnsGrid}>
            {filteredImages
              .slice(currentPage * 8, currentPage * 8 + 8) // slice only 8 per page
              .map((item: GalleryItem) => (
                <GalleryThumbnail
                  key={item?.id}
                  item={item}
                  domain={API_ENDPOINT || "http://localhost:3000"}
                  setFullScreen={() => {
                    setSelectedImage(
                      images.filter((image) => image.title === item.title)
                    );
                  }}
                  setSelectedImage={() => {
                    setFullscreen(true);
                  }}
                ></GalleryThumbnail>
              ))}
          </div>
        </div>
      </div>
      <div className={styles.pagination}>
        {Array.from(
          { length: Math.ceil(filteredImages.length / 8) },
          (_, i) => {
            return (
              <button
                className={`${styles.paginationButton} ${currentPage === i && styles.paginationButtonActive}`}
                key={i}
                onClick={() => {
                  setCurrentPage(i);
                }}
              >
                {i + 1}
              </button>
            );
          }
        )}
      </div>
    </>
  );
}

export default GalleryGrid;
