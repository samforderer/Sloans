// @ts-nocheck
import { useForm, Controller } from "react-hook-form";
import mainStyles from "../styles/App.module.css";
import formStyles from "../styles/Form.module.css";
import { motion } from "framer-motion";
import Button from "./Button";
import { zodResolver } from "@hookform/resolvers/zod";
import { TQuoteSchema, quoteSchema } from "../lib/types";
import { useEffect, useRef, useState } from "react";
import { FaCheck } from "react-icons/fa";
import { AddressAutofill } from "@mapbox/search-js-react";

const API_ENDPOINT = `${import.meta.env.VITE_API_ENDPOINT}/quote/new`

const Form = () => {
  const {
    register,
    handleSubmit,
    reset,
    control,
    formState: { errors, isSubmitting },
  } = useForm<TQuoteSchema>({
    resolver: zodResolver(quoteSchema),
  });

  const [isSubmitted, setIsSubmitted] = useState(false);
  const formContainer = useRef<HTMLElement>(null);

  useEffect(() => {
    // change mapbox input width on load
    const autofillInput = document.getElementsByTagName(
      "mapbox-address-autofill"
    ) as HTMLCollectionOf<HTMLElement>;
    if (autofillInput[0]) {
      autofillInput[0].style.width = "80%";
      autofillInput[0].style.marginTop = "0";
      autofillInput[0].style.marginLeft = "auto";
      autofillInput[0].style.marginRight = "auto";
    }
  });

  const onSubmit = async (data: TQuoteSchema) => {
    // to do submit to server
    console.log(data);
    const response = await fetch(API_ENDPOINT, {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (!response.ok) {
      alert("Something went wrong! Please try again soon.");
      return;
    }

    setIsSubmitted(true);

    setTimeout(() => {
      setIsSubmitted(false);
    }, 2000);

    reset();
  };

  return (
    <section
      ref={formContainer}
      className={`${mainStyles.greyContainer} ${mainStyles.container} ${isSubmitted && formStyles.success}`}
    >
      {!isSubmitted ? (
        <>
          <h1 className={mainStyles.heading1}>Get a Quote</h1>
          <form
            onSubmit={handleSubmit(onSubmit)}
            className={formStyles.form}
            name="quote"
          >
            <input
              {...register("first_name")}
              className={formStyles.textBox}
              type="text"
              name="first_name"
              placeholder="your first name"
            />
            {errors.first_name && (
              <span className={formStyles.errorMessage}>
                {errors.first_name.message}
              </span>
            )}

            <input
              {...register("last_name")}
              className={formStyles.textBox}
              type="text"
              name="last_name"
              placeholder="your last name"
            />
            {errors.last_name && (
              <span className={formStyles.errorMessage}>
                {errors.last_name.message}
              </span>
            )}

            <input
              {...register("email")}
              className={formStyles.textBox}
              type="email"
              name="email"
              placeholder="youremail@email.com"
            />
            {errors.email && (
              <span className={formStyles.errorMessage}>
                {errors.email.message}
              </span>
            )}

            <input
              {...register("phone")}
              className={formStyles.textBox}
              type="tel"
              name="phone"
              placeholder="905-123-4567"
              pattern="[0-9]{3}[0-9]{3}[0-9]{4}"
            />
            {errors.phone && (
              <span className={formStyles.errorMessage}>
                {errors.phone.message}
              </span>
            )}

            {/* Address input */}
            <AddressAutofill
              style={{ width: "1000px" }}
              accessToken={import.meta.env.VITE_MAPBOX_TOKEN}
            >
              <Controller
                control={control}
                name="address"
                render={({ field: { onChange } }) => (
                  <input
                    type="text"
                    onChange={onChange} // send value to hook form
                    className={formStyles.textBox}
                    style={{
                      width: "100%",
                    }}
                    name="address"
                    placeholder="123 Cherrylane Blvd"
                    autoComplete="street-address"
                  />
                )}
              />
            </AddressAutofill>
            {errors.address && (
              <span className={formStyles.errorMessage}>
                {errors.address.message}
              </span>
            )}

            <div className={formStyles.servicesContainer}>
              <h2>Services Required</h2>
              <div className={formStyles.servicesSelect}>
                <label htmlFor="painting" className={formStyles.label}>
                  <input
                    {...register("painting")}
                    type="checkbox"
                    name="painting"
                    id="painting"
                  />
                  PAINTING
                </label>
                <label htmlFor="renovations" className={formStyles.label}>
                  <input
                    {...register("renovations")}
                    type="checkbox"
                    name="renovations"
                    id="renovations"
                  />
                  RENOVATIONS
                </label>
              </div>
            </div>
            <textarea
              {...register("quote_description")}
              className={formStyles.textArea}
              name="quote_description"
              placeholder="a brief description of the job you need done."
            ></textarea>
            {errors.quote_description && (
              <span className={formStyles.errorMessage}>
                {errors.quote_description.message}
              </span>
            )}

            <Button
              disabled={isSubmitting}
              text={isSubmitting ? "Loading..." : "Submit"}
            />
          </form>
        </>
      ) : (
        <>
          <motion.div
            initial={{ opacity: 0, scale: 0.5 }}
            animate={{ opacity: 1, scale: 1 }}
            transition={{ duration: 0.5 }}
          >
            <FaCheck
              className={mainStyles.heading1}
              style={{ marginBottom: "0" }}
            ></FaCheck>
          </motion.div>
          <h1 className={mainStyles.heading1} style={{ marginBottom: "0" }}>
            Thank you!
          </h1>
          <p className={mainStyles.bodyText}>We will be in touch shortly!</p>
        </>
      )}
    </section>
  );
};

export default Form;
