import styles from "../styles/Button.module.css";

interface ButtonProps {
  text: string;
  disabled: boolean;
  color?: "green" | "rust";
  position?: "left" | "right";
  children?: any;
}

const Button = ({ text, disabled, color, position, children }: ButtonProps) => {
  return (
    <button
      disabled={disabled}
      type="submit"
      className={`${styles.button} ${color == "green" ? styles.green : ""}`}
      style={
        position
          ? position == "right"
            ? { marginLeft: "auto" }
            : { marginRight: "auto" }
          : {}
      }
    >
      {text}
      {children}
    </button>
  );
};

export default Button;
