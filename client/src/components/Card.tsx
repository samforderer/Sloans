import mainStyles from "../styles/App.module.css";
import servicesStyles from "../styles/Services.module.css";

interface CardProps {
  heading?: string;
  body?: string;
  children?: React.ReactNode;
  className?: React.CSSProperties;
}

const Services = ({ children, heading, body, className }: CardProps) => {
  return (
    <section
      className={`${mainStyles.greyContainer} ${mainStyles.container} `}
      style={className}
    >
      {heading && <h1 className={mainStyles.heading1}>{heading}</h1>}
      {body && (
        <p className={`${mainStyles.bodyText} ${servicesStyles.servicesPara}`}>
          {body}
        </p>
      )}
      {children}
    </section>
  );
};

export default Services;
