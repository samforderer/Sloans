import styles from "../styles/Footer.module.css";

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <p>Sloan's Paint 'n Fix 2022 Ⓒ</p>
    </footer>
  );
};

export default Footer;
