import styles from "../styles/Skeleton.module.css";

const SkeletonImg = () => {
  return <div className={styles.skeleton}></div>;
};

export default SkeletonImg;
