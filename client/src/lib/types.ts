import { z } from "zod";

export const quoteSchema = z.object({
  first_name: z
    .string()
    .trim()
    .min(1, { message: "first name required" })
    .regex(/^[a-zA-Z]+$/, { message: "no numbers or special characters" }),
  last_name: z
    .string()
    .trim()
    .min(1, { message: "last name required" })
    .regex(/^[a-zA-Z]+$/, { message: "no numbers or special characters" }),
  email: z.string().email({ message: "email required" }),
  phone: z.string().regex(/^\d{10}|(\(?\d{3}\)?[-.\s]?\d{3}[-.\s]\d{4})$/, {
    message: "must be a valid phone number",
  }),
  quote_description: z.string(),
  painting: z.boolean().optional(),
  renovations: z.boolean().optional(),
  address: z.string().min(1, { message: "address required" }),
});

export interface GalleryItem {
  id: number;
  url: string;
  title: string;
  description: string;
}

export type TQuoteSchema = z.infer<typeof quoteSchema>;
