import Header from "../components/Header";
import Footer from "../components/Footer";
import Services from "../components/services/Services";

function ServicesPage() {
  return (
    <>
      <Header activePage={"services"} />
      <Services></Services>
      <Footer />
    </>
  );
}

export default ServicesPage;
