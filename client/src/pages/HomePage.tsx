import Header from "../components/Header";
import Form from "../components/Form";
import Footer from "../components/Footer";
import Hero from "../components/Hero";
import ServicesHome from "../components/services/ServicesHome";
import React from "react";

function HomePage() {
  const servicesRef = React.useRef(null);

  return (
    <div id="container">
      <Header activePage={"home"} />
      <Hero servicesRef={servicesRef}></Hero>
      <ServicesHome servicesRef={servicesRef}></ServicesHome>
      <Form />
      <Footer />
    </div>
  );
}

export default HomePage;
