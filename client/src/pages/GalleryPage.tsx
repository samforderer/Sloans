import Footer from "../components/Footer.tsx";
import Header from "../components/Header.tsx";
import Gallery from "../components/gallery/Gallery.tsx";

function GalleryPage() {
  return (
    <>
      <Header activePage={"gallery"} />
      <Gallery></Gallery>
      <Footer />
    </>
  );
}

export default GalleryPage;
