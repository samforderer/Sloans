import Footer from "../components/Footer";
import Header from "../components/Header";
import Contact from "../components/Contact"

const ContactPage = () => {
  return (
    <>
      <Header activePage={"contact"} />
      <Contact></Contact>
      <Footer />
    </>
  )
}

export default ContactPage
