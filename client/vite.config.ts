import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import vsharp from "vite-plugin-vsharp";
import path from "path";
import { fileURLToPath } from "url";
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    vsharp({
      width: 800, // Max width, images with a smaller width than this will not be resized
      height: 800, // Max height, images with a smaller height than this will not be resized
      scale: 0.8, // Overrides width and height
    }),
  ],
});
