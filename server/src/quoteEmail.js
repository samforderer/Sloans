const quoteSubmissionEmail = (mailer, quote) => {
  const messageBody = `First Name: ${quote.first_name},
    Last Name: ${quote.last_name},
    Email: ${quote.email},
    Phone: ${quote.phone},
    Quote Description: ${quote.quote_description},
    Painting: ${quote.painting},
    Renovations: ${quote.renovations},
    Address: ${quote.address}`;

  mailer.sendMail(
    {
      to: "samsloanforderer@icloud.com",
      subject: "example",
      text: "test",
    },
    (errors) => {
      if (errors) {
        fastify.log.error(errors);
        return errors;
      }
    }
  );
};

export default quoteSubmissionEmail;
