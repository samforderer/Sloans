const postSchema = {
  body: {
    type: "object",
    properties: {
      first_name: { type: "string" },
      last_name: { type: "string" },
      email: { type: "string", format: "email" },
      phone: { type: "string", minLength: 10, maxLength: 10 },
      quote_description: { type: "string" },
      painting: { type: "boolean" },
      renovations: { type: "boolean" },
      address: {
        type: "string",
      },
    },
    required: [
      "first_name",
      "last_name",
      "email",
      "phone",
      "quote_description",
      "address",
    ],
  },
};

const quoteController = (fastify, { postSchema }, done) => {
  fastify.post("/new", async (req, reply) => {
    const quote = req.body;
    try {
      await fastify.mysql.execute(
        `
    INSERT INTO quote_request (first_name, last_name, email, phone, quote_description, painting, renovations, address)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?);
    `,
        [
          quote.first_name,
          quote.last_name,
          quote.email,
          quote.phone,
          quote.quote_description,
          quote.painting,
          quote.renovations,
          quote.address,
        ]
      );

      // EMAIL NOTIFICATION
      const { mailer } = fastify;
      const messageBody =
        `First Name: ${quote.first_name}\n` +
        `Last Name: ${quote.last_name}\n` +
        `Email: ${quote.email}\n` +
        `Phone: ${quote.phone}\n` +
        `Quote Description: ${quote.quote_description}\n` +
        `Painting: ${quote.painting}\n` +
        `Renovations: ${quote.renovations}\n` +
        `Address: ${quote.address}`;

      mailer.sendMail(
        {
          to: process.env.SENDTO_EMAIL,
          subject: "New Quote Submission",
          text: messageBody,
        },
        (errors) => {
          if (errors) {
            fastify.log.error(errors);
            reply.code(500).send({ error: "An unexpected error occurred" });
          }
        }
      );

      reply
        .code(200)
        .header("Content-Type", "application/json; charset=utf-8")
        .send({ message: "QUOTE SUBMISSION SUCCESS!" });
    } catch (error) {
      console.log(error);
    }
  });

  fastify.get("/quotes", async (req, reply) => {
    const [quotes] = await fastify.mysql.execute("SELECT * FROM quote_request");
    return { quotes };
  });

  done();
};

export default quoteController;
