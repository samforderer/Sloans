const galleryController = (fastify, options, done) => {

  fastify.get("/", async (req, reply) => {
    const [gallery] = await fastify.mysql.execute("SELECT * FROM gallery");
    return { gallery };
  });

  done();
};

export default galleryController;
