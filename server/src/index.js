import Fastify from "fastify";
import fastifyStatic from "@fastify/static";
import fastifyMysql from "@fastify/mysql";
import fp from "fastify-mailer";
import galleryController from "./gallery-controller.js";
import quoteController from "./quote-controller.js";

// to get directory path
import path from "path";
import { fileURLToPath } from "url";
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const fastify = Fastify({
  logger: true,
});

// Serve static page index.html
fastify.register(fastifyStatic, {
  root: path.join(__dirname, "../../client/dist"),
  prefix: "/", // optional: default '/'
});

// Serve gallery images
fastify.register(fastifyStatic, {
  root: path.join(__dirname, "../images/gallery/"),
  prefix: "/gallery",
  decorateReply: false,
});

// Database Access
console.log(process.env.MYSQL_USER);
const mysqlPassword = process.env.MYSQL_ROOT_PASSWORD;
fastify.register(fastifyMysql, {
  host: "sloans-db-1",
  user: process.env.MYSQL_USER,
  password: mysqlPassword,
  database: "db",
  promise: true,
});

fastify.register(fp, {
  defaults: { from: `Sloans Noreply <${process.env.SENDFROM_EMAIL}>` },
  transport: {
    host: process.env.EMAIL_HOST,
    port: 465,
    secure: true, // use TLS
    auth: {
      user: process.env.SENDFROM_EMAIL,
      pass: process.env.SENDFROM_EMAIL_PASSWORD,
    },
  },
});

// Route not found
fastify.setNotFoundHandler((req, res) => {
  res.sendFile("index.html");
});

// Controllers
fastify.register(quoteController, { prefix: "/quote" });
fastify.register(galleryController, { prefix: "/services/gallery" });

// Run server
try {
  fastify.listen({ port: 3000, host: "0.0.0.0" });
} catch (error) {
  fastify.log.error(error);
  process.exit(1);
}
