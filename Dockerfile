FROM node:19

# Define build arguments for environment variables
ARG VITE_API_ENDPOINT
ARG VITE_MAPBOX_TOKEN

# Set environment variables during the build process
ENV VITE_API_ENDPOINT=$VITE_API_ENDPOINT
ENV VITE_MAPBOX_TOKEN=$VITE_MAPBOX_TOKEN

WORKDIR /app

COPY ./server/package*.json ./server/
RUN cd server/ && npm install

COPY ./client/package*.json ./client/
RUN cd client/ && npm ci

COPY server ./server
COPY client ./client
RUN cd client/ && npm run build
ENV VITE_API_BASE_URL=$VITE_API_BASE_URL
ENV VITE_API_KEY=$VITE_API_KEY
EXPOSE 3000

CMD ["node", "server/src/index.js"]
