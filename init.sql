CREATE TABLE IF NOT EXISTS quote_request (
    id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    email VARCHAR(200) NOT NULL,
    submitted_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    phone CHAR(10) NOT NULL,
    quote_description VARCHAR(500) NOT NULL,
    painting BOOLEAN,
    renovations BOOLEAN,
    address VARCHAR(250) NOT NULL
);

CREATE TABLE IF NOT EXISTS gallery (
    id INT AUTO_INCREMENT PRIMARY KEY,
    url VARCHAR(200) NOT NULL,
    title VARCHAR(100) NOT NULL,
    description TEXT NOT NULL
);

INSERT INTO gallery (url, title, description) VALUES
('gallery/reno_1_1.webp', 'Bathroom Ceiling Leak Repair', 'Bathroom Ceiling Leak Repair'),
('gallery/reno_1_2.webp', 'Bathroom Ceiling Leak Repair', 'Bathroom Ceiling Leak Repair'),
('gallery/reno_2_1.webp', 'Hallway Leak Repair', 'Hallway leak repair in condo building.'),
('gallery/reno_2_2.webp', 'Hallway Leak Repair', 'Hallway leak repair in condo building.'),
('gallery/reno_3_1.webp', 'Basement Renovation', 'Basement Renovation'),
('gallery/reno_3_2.webp', 'Basement Renovation', 'Basement Renovation'),
('gallery/reno_4_1.webp', 'Wind/Snow Guard Installation', 'Wind/snow guard for condo automatic door.'),
('gallery/reno_4_2.webp', 'Wind/Snow Guard Installation', 'Wind/snow guard for condo automatic door.'),
('gallery/reno_5_1.webp', 'Condo Fencing Project', 'Condo Fencing Project'),
('gallery/painting_1_1.webp', 'Stairwell Painting', 'Condo painting project. This project included 12 floors of stairwell painting');
